title:introduction of kobe bryant
date:2020.08.06

# Title:Introduction of Kobe Bryant

1. 2006年面對暴龍砍81分
>[![Click me to watch the film!](/upload/ask/29/W/WebMole_Youtube_Video.png)](https://www.youtube.com/watch?v=o9NILK4OXpo)  
2. 2000年拿下生涯首冠
>[![Click me to watch the film!](/upload/ask/29/W/WebMole_Youtube_Video.png)](https://www.youtube.com/watch?v=Vy6LSSslMCE)  
3. 2013年阿基里斯腱斷裂
>[![Click me to watch the film!](/upload/ask/29/W/WebMole_Youtube_Video.png)](https://www.youtube.com/watch?v=zKiQsg-8nEM)  

>* 生涯數據
<table border="5">
     <tr>
     <th>生涯總得分</th>
     <th>生涯總助攻</th>
     <th>生涯總籃板</th>
     <th>生涯總抄截</th>
     <th>生涯總阻攻</th>
     </tr>
     <tr>
     <td>33643</td>
     <td>6306</td>
     <td>7047</td>
     <td>1944</td>
     <td>640</td>
     </tr>
     </table>


* Music  
>![Sample Audio](img/markdown_audio.mp3)

>* 無序清單  
>##Kobe  
>###Kobe  
>####Kobe  

>## [Week5 HW](https://data.depositar.io/dataset/test_cody)
